'use strict';

import * as _ from './helper';

(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return (root.jsCustomSelect = factory());
        });
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.jsCustomSelect = factory();
    }
}(window, function () {
    const root = window;
    const _document = root.document;

    function jsCustomSelect(select, userSettings) {

        const Default = {
            initClass: 'field-dropdown',
            search: false,
            searchInputHeight: 36,
            searchNoResults: 'No results match'
        };

        const options = Object.assign(Default, userSettings);

        let input_opt = select.getAttribute('data-options');
        if (input_opt) {
            try {
                let _options = JSON.parse(input_opt);
                for (let i in _options) {
                    options[i] = _options[i];
                }
            } catch (e) {
                console.error(e);
            }
        }

        var classStr = select.className.replace(/\bjs-custom-select ?\b/g, "");
        var optSelected = select.querySelector('option[selected]');
        var selectBlock, searchBlock, UL, selected, scroll, searchInput, At_Fn_enabled, scrollTop = 0;
        var selectedElem = optSelected && optSelected.value;
        var LI = select.querySelectorAll('option');

        const domSelectBlockClose = event => {

            let target = event.target;

            if (selectBlock !== target.closest('.' + options.initClass) || (!target.classList.contains(options.initClass) && !target.closest('.' + options.initClass))) {
                selectBlockClose(target);
            }
        };

        const selectBlockOpen = event => {

            if (select.classList.contains('disabled')) {
                return;
            }

            if (options['max-height']) {

                let doc = _document.documentElement;

                let selectBlockCoords = selectBlock.getBoundingClientRect();
                let top = root.pageYOffset + (selectBlockCoords.top || 0 + selectBlockCoords.height);
                let docHeight = doc.scrollHeight || doc.clientHeight;

                let selectBlockData = top + selectBlockCoords.height + options.searchInputHeight + 15;
                if (docHeight < (selectBlockData + options['max-height'])) {
                    options['max-height'] = docHeight - selectBlockData;
                }
                UL.style.maxHeight = options['max-height'] + 'px';
            }

            if (selectBlock.classList.contains('open')) {
                selectBlockClose();
            } else {
                selectBlock.classList.add('open');

                if (scrollTop && typeof scroll === 'object' && typeof scroll.setOptions === 'function') {
                    scroll.setScrollTop(scrollTop);
                }

                if (typeof scroll === 'object' && typeof scroll.update === 'function') {
                    UL.style.overflowY = 'hidden';
                    scroll.update();
                } else {
                    UL.style.overflowY = 'scroll';
                }
            }

            if (_.browser.addEventListener) {
                if (_.browser.touch) {
                    root.addEventListener('touchend', domSelectBlockClose, false);
                } else {
                    root.addEventListener('mouseup', domSelectBlockClose, false);
                }
            } else {
                root.onclick = domSelectBlockClose;
            }
        };

        const selectBlockClose = () => {

            if (typeof scroll === 'object' && typeof scroll.getOptions === 'function') {
                scrollTop = scroll.getOptions()['scrollTop'];
            }

            if (UL && options.search && searchInput) {
                searchInput.value = '';
                clearUl();
                createSelectListItem();
                if (typeof scroll === 'object' && At_Fn_enabled) {
                    if (typeof scroll.setOptions === 'function') {
                        scroll.setOptions({
                            At_Fn_enabled: At_Fn_enabled
                        });
                    }
                }
            }

            let selectBlockOpen = root.document.querySelectorAll('.' + options.initClass + '.open');

            for (var i = 0; i < selectBlockOpen.length; i++) {
                selectBlockOpen[i].classList.remove('open');
            }

            if (_.browser.addEventListener) {
                if (_.browser.touch) {
                    root.removeEventListener('touchend', domSelectBlockClose, false);
                } else {
                    root.removeEventListener('mouseup', domSelectBlockClose, false);
                }
            } else {
                root.onclick = null;
            }
        };

        const selectListClick = event => {

            let target = event.target;

            if (selected.innerHTML === target.innerHTML) {
                return;
            }

            let _value = target.getAttribute('data-value');
            selected.innerHTML = stripHtml(target.innerHTML);
            if (optSelected) {
                optSelected.removeAttribute('selected');
            }

            selectedElem = select.querySelector('option[value="' + _value + '"]');
            if (!selectedElem) {
                console.error('The selected item was not found.');
                selectBlockClose();
                return;
            }

            selectedElem.selected = true;
            selectedElem.setAttribute('selected', 'selected');

            if ("createEvent" in root.document) {
                let evt = _document.createEvent("HTMLEvents");
                evt.initEvent("change", false, true);
                select.dispatchEvent(evt);
            } else {
                select.fireEvent("onchange");
            }

            if (typeof options.change === 'function') {
                setTimeout(options.change, 0, selectedElem, target);
            }

            selectBlockClose();
        };

        const createSelectListItem = search => {

            let count = 0;
            for (var i = 0; i < LI.length; i++) {

                let opt = LI[i].cloneNode(true);

                if (options.search && search) {
                    let pos;
                    let html = stripHtml(opt.innerHTML);
                    if ((pos = html.indexOf(search)) === -1) {
                        continue;
                    }
                    let searchLength = search.length;
                    let _start = html.substr(0, pos);
                    let _end = html.substr(pos + searchLength);

                    opt.innerHTML = _start + '<em>' + html.substr(pos, searchLength) + '</em>' + _end;
                }


                let li = _.createTag('li', {
                    "class": 'select-list-item',
                    "data-value": opt.value
                });
                li.innerHTML = opt.innerHTML;

                if (selected.innerHTML === li.innerHTML) {
                    li.classList.add('selected');
                }

                if (!opt.disabled) {
                    if (_.browser.addEventListener) {
                        if (_.browser.touch) {
                            li.removeEventListener('touchend', elementEevents, false);
                            li.addEventListener('touchend', elementEevents, false);
                        } else {
                            li.removeEventListener('mouseup', elementEevents, false);
                            li.addEventListener('mouseup', elementEevents, false);
                            li.removeEventListener('mousedown', elementEevents, false);
                            li.addEventListener('mousedown', elementEevents, false);
                            li.removeEventListener('mouseenter', elementEevents, false);
                            li.addEventListener('mouseenter', elementEevents, false);
                            li.removeEventListener('mouseout', elementEevents, false);
                            li.addEventListener('mouseout', elementEevents, false);
                        }
                    } else {
                        li.onclick = elementEevents;
                        li.onmouseenter = elementEevents;
                        li.onmouseout = elementEevents;
                    }

                } else {
                    li.classList.add('disabled');
                    li.setAttribute('disabled', '');
                }
                count++;
                UL.appendChild(li);
            }

            return count;

        };

        const searchInputEvent = event => {

            clearUl();
            let val = event.target.value;

            if (options.search && typeof scroll === 'object') {
                if (val.length) {
                    if (At_Fn_enabled === undefined && typeof scroll.getOptions === 'function') {
                        At_Fn_enabled = scroll.getOptions()['At_Fn_enabled'];
                    }
                    if (typeof scroll.setOptions === 'function') {
                        scroll.setOptions({
                            At_Fn_enabled: false
                        });
                    }
                } else if (At_Fn_enabled) {
                    if (typeof scroll.setOptions === 'function') {
                        scroll.setOptions({
                            At_Fn_enabled: true
                        });
                    }
                }

                if (!val.length && scrollTop && typeof scroll.setOptions === 'function') {
                    setTimeout(scroll.setScrollTop, 50, scrollTop);
                }
            }

            let count = createSelectListItem(val);

            if (!count) {
                let li = _.createTag('li', {
                    "class": 'select-list-item'
                });
                li.innerHTML = options.searchNoResults + ' "' + val + '"';
                li.style.cursor = 'default';
                UL.appendChild(li);
            }

        };

        const kill = () => {

            selectBlockClose();

            let nextSibling = select.nextSibling;
            while (nextSibling && nextSibling.nodeType !== 1) {
                nextSibling = nextSibling.nextSibling;
            }

            if (nextSibling && nextSibling.classList.contains(options.initClass)) {
                nextSibling.remove();
            }

        };

        var isMousemove;
        var elementEevents = {
            handleEvent: function (event) {
                let target = event.target;

                switch (event.type) {
                    case 'mouseenter':
                        target.classList.add('hover');
                        break;
                    case 'mouseout':
                        target.classList.remove('hover');
                        break;
                    case 'mousedown':
                        this.start(event);
                        break;
                    case 'mousemove':
                    case 'touchmove':
                        if (!isMousemove) {
                            let child = UL.children;
                            for (var i = 0; i < child.length; i++) {
                                child[i].removeEventListener('mouseenter', elementEevents, false);
                                child[i].removeEventListener('mouseout', elementEevents, false);
                            }
                            target.classList.remove('hover');
                            isMousemove = true;
                        }
                        break;
                    case 'touchend':
                    case 'mouseup':
                    case 'mouseleave':
                        this.end(event);
                        break;
                }

            },

            start: function (event) {
                root.addEventListener('mousemove', this, true);
                root.addEventListener('mouseup', this, true);
                root.addEventListener('mouseleave', this, true);
            },

            end: function (event) {

                if (isMousemove) {
                    let child = UL.children;
                    for (var i = 0; i < child.length; i++) {
                        child[i].addEventListener('mouseenter', elementEevents, false);
                        child[i].addEventListener('mouseout', elementEevents, false);
                    }
                    event.target.classList.add('hover');
                    isMousemove = false;
                } else {
                    selectListClick(event);
                }

                root.removeEventListener('mousemove', this, true);
                root.removeEventListener('mouseup', this, true);
                root.removeEventListener('mouseleave', this, true);

            }
        };


        const update = () => {
            clearUl();
            LI = select.querySelectorAll('option');
            createSelectListItem();
        };

        const init = () => {

            kill();

            selectBlock = _.createTag('div', {
                class: options.initClass + (classStr ? ' ' + classStr : '')
            });

            let valueField = _.createTag('span', {
                class: 'value-field'
            });

            valueField.onclick = selectBlockOpen;

            selected = _.createTag('span', {
                "class": 'value'
            });

            let defaultVal = options.defaultVal || (optSelected && optSelected.innerHTML) || options.placeholder;
            selected.innerHTML = defaultVal;

            let fa = _.createTag('div', {
                "class": 'open-icon'
            });

            valueField.appendChild(selected);
            valueField.appendChild(fa);

            selectBlock.appendChild(valueField);

            UL = _.createTag('ul', {
                class: 'field-dropdown-list'
            });

            if (options.search) {

                searchBlock = _.createTag('span', {
                    "class": 'search-block fa'
                });
                searchInput = _.createTag('input', {
                    "class": 'search-input',
                    "placeholder": 'Search'
                });
                searchInput.style.height = options.searchInputHeight + 'px';

                searchInput.onclick = e => {
                    if (typeof scroll === 'object' && typeof scroll.getOptions === 'function') {
                        scrollTop = scroll.getOptions()['scrollTop'];
                    }
                };

                searchInput.oninput = searchInputEvent;
                searchBlock.appendChild(searchInput);
                selectBlock.appendChild(searchBlock);
            }

            createSelectListItem();

            selectBlock.appendChild(UL);
            select.parentNode.insertBefore(selectBlock, select.nextSibling);

            if (typeof jsCustomScroll === 'function') {
                scroll = new jsCustomScroll(UL, options.scroll);
            }

            return {
                getSelect: () => {
                    return select;
                },
                selt: () => {
                    return selectBlock;
                },
                getSelected: () => {
                    return selectedElem;
                },
                update: update,
                kill: kill
            };
        };

        const clearUl = () => {

            let selectListLi = UL.getElementsByTagName('li');
            let i = 0;
            let count = 0;
            while (selectListLi[i]) {
                let element = selectListLi[i];
                if (element.parentNode) {
                    element.parentNode.removeChild(element);
                }
                if (count > 2000) {
                    console.error('Emergency stop!');
                    break;
                }
                count++;
            }
        };

        const stripHtml = el => {
            return el.replace(/<(?:.|\n)*?>/gm, '');
        };

        return init();
    }
    if (root.jQuery || root.Zepto) {
        (function ($) {
            $.fn.jsCustomSelect = function (params) {
                return this.each(function () {
                    $(this).data('jsCustomSelect', new jsCustomSelect($(this)[0], params || []));
                });
            };
        })(root.jQuery || root.Zepto);
    }
    return jsCustomSelect;
}));
